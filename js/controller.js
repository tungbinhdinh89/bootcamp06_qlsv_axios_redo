// 1. get information from form
export let getInforFromForm = () => {
  let id = document.getElementById('txtMaSV').value;
  let name = document.getElementById('txtTenSV').value;
  let email = document.getElementById('txtEmail').value;
  let password = document.getElementById('txtPass').value;
  let scoreOfMath = document.getElementById('txtDiemToan').value * 1;
  let scoreOfPhysic = document.getElementById('txtDiemLy').value * 1;
  let scoreOfChermical = document.getElementById('txtDiemHoa').value * 1;
  return {
    id,
    name,
    email,
    password,
    scoreOfMath,
    scoreOfPhysic,
    scoreOfChermical,
  };
};

// render list of student
export let renderStudentList = (studentArr) => {
  let contentHTML = '';
  studentArr.forEach((item) => {
    let contentTr = `
    <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.email}</td>
    <td>${
      (item.scoreOfMath * 1 +
        item.scoreOfPhysic * 1 +
        item.scoreOfChermical * 1) /
      3
    }</td>
    <td><button class='btn btn-danger' onclick='deleteStudent(${
      item.id
    })'>Xoá</button></td>
    <td><button class='btn btn-warning' onclick='editStudent(${
      item.id
    })'>Sửa</button></td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById('tbodySinhVien').innerHTML = contentHTML;
};

// push infor to form
export let pushInforToForm = (inforStudent) => {
  document.getElementById('txtMaSV').value = inforStudent.id;
  document.getElementById('txtTenSV').value = inforStudent.name;
  document.getElementById('txtEmail').value = inforStudent.password;
  document.getElementById('txtPass').value = inforStudent.email;
  document.getElementById('txtDiemToan').value = inforStudent.scoreOfMath;
  document.getElementById('txtDiemLy').value = inforStudent.scoreOfPhysic;
  document.getElementById('txtDiemHoa').value = inforStudent.scoreOfChermical;
};

// thông báo
export let onSuccess = (messeager) => {
  Toastify({
    text: messeager,
    duration: 3000,
    destination: 'https://github.com/apvarun/toastify-js',
    newWindow: true,
    close: true,
    gravity: 'top', // `top` or `bottom`
    position: 'left', // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: 'linear-gradient(to right, #00b09b, #96c93d)',
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};

// loading

export let onLoading = () => {
  document.getElementById('loading').style.display = 'flex';
};

export let offLoading = () => {
  document.getElementById('loading').style.display = 'none';
};
