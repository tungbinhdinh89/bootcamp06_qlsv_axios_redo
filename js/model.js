export class Student {
  constructor(
    id,
    name,
    email,
    password,
    scoreOfMath,
    scoreOfPhysic,
    scoreOfChermical
  ) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.scoreOfMath = scoreOfMath;
    this.scoreOfPhysic = scoreOfPhysic;
    this.scoreOfChermical = scoreOfChermical;
  }
}
