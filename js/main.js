import { getInforFromForm } from './controller.js';
import { renderStudentList } from './controller.js';
import { pushInforToForm } from './controller.js';
import { onSuccess } from './controller.js';
import { onLoading } from './controller.js';
import { offLoading } from './controller.js';
import { Student } from './model.js';
getInforFromForm();
console.log('getInforFromForm(): ', getInforFromForm());

const BASE_URL = `https://63f442adfe3b595e2ef03850.mockapi.io`;

// es6 arrow function remember declare let before function
let fetchStudentList = () => {
  // tải trang on loading
  onLoading();
  axios({
    url: `${BASE_URL}/Student`,
    method: `get`,
  })
    .then(function (res) {
      // chạy thành công tắt loading
      renderStudentList(res.data);
      offLoading();
      console.log('res: ', res);
    })
    .catch(function (err) {
      // lỗi cũng tắt loading
      offLoading();
      console.log('err: ', err);
    });
};
fetchStudentList();

// delete student
let deleteStudent = (id) => {
  // tải trang on loading
  onLoading();
  axios({
    url: `${BASE_URL}/Student/${id}`,
    method: `delete`,
  })
    .then((res) => {
      console.log(res);
      fetchStudentList();
      onSuccess('Delete Success');
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};
window.deleteStudent = deleteStudent;

// edit student

let editStudent = (id) => {
  onLoading();

  axios({
    url: `${BASE_URL}/Student/${id}`,
    method: 'get',
  })
    .then((res) => {
      pushInforToForm(res.data);
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};
window.editStudent = editStudent;

// update student

let updateStudent = () => {
  onLoading();

  axios({
    url: `${BASE_URL}/Student/${getInforFromForm().id}`,
    method: `put`,
    data: getInforFromForm(),
  })
    .then((res) => {
      fetchStudentList();
      onSuccess('Update Success');
      offLoading();

      console.log(res);
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

window.updateStudent = updateStudent;

// add Student
let addStudent = () => {
  onLoading();

  axios({
    url: `${BASE_URL}/Student/`,
    method: `post`,
    data: getInforFromForm(),
  })
    .then((res) => {
      fetchStudentList();
      onSuccess('Add Student Success');
      offLoading();
    })
    .catch((err) => {
      console.log(err);
      offLoading();
    });
};

window.addStudent = addStudent;
