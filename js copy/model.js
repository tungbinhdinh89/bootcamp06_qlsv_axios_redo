function SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa) {
  this.maSV = maSV;
  this.tenSV = tenSV;
  this.email = email;
  this.matKhau = matKhau;
  this.diemToan = diemToan;
  this.diemLy = diemLy;
  this.diemHoa = diemHoa;
  this.tinhDTB = function () {
    return (
      (Number(this.diemToan) + Number(this.diemLy) + Number(this.diemHoa)) / 3
    );
  };
}
