// 1. dom ID
domID = (id) => {
  return document.getElementById(id);
};
// 2. function render list of student
renderListStudent = (listStudent) => {
  let contentHTML = '';
  for (var i = listStudent.length - 1; i >= 0; i--) {
    item = listStudent[i];
    let contentTR = `
    <tr>
    <td>${item.idStudent}</td>
    <td>${item.nameStudent}</td>
    <td>${item.email}</td>
    <td>${tinhDTB()}</td>
    <td><button class='btn btn-danger' onclick='deleteStudent(${
      item.idStudent
    })'>Xoá</button></td>
    <td><button class='btn btn-warning' onclick='editStudent(${
      item.idStudent
    })'>Sửa</button></td>
    </tr>
    `;
    contentHTML += contentTR;
  }
  domID('tbodySinhVien').innerHTML = contentHTML;
};

// 3. Fill Information From Data To Form
fillInforFromDataToForm = (inforStudent) => {
  domID('txtMaSV').value = inforStudent.idStudent;
  domID('txtTenSV').value = inforStudent.nameStudent;
  domID('txtEmail').value = inforStudent.email;
  domID('txtPass').value = inforStudent.password;
  domID('txtDiemToan').value = inforStudent.scoreOfMath;
  domID('txtDiemLy').value = inforStudent.scoreOfPhysic;
  domID('txtDiemHoa').value = inforStudent.scoreOfChermical;
};
// 4. get information from form
getInforFromForm = () => {
  var idStudent = domID('txtMaSV').value;
  var nameStudent = domID('txtTenSV').value;
  var email = domID('txtEmail').value;
  var password = domID('txtPass').value;
  var scoreOfMath = domID('txtDiemToan').value * 1;
  var scoreOfPhysic = domID('txtDiemLy').value * 1;
  var scoreOfChermical = domID('txtDiemHoa').value * 1;
  return {
    idStudent: idStudent,
    nameStudent: nameStudent,
    email: email,
    password: password,
    scoreOfMath: scoreOfMath,
    scoreOfPhysic: scoreOfPhysic,
    scoreOfChermical: scoreOfChermical,
  };
};

// tinhDTB = () => {
//   var scoreOfMath = domID('txtDiemToan').value * 1;
//   var scoreOfPhysic = domID('txtDiemLy').value * 1;
//   var scoreOfChermical = domID('txtDiemHoa').value * 1;
//   return (
//     (inforStudent.scoreOfMath +
//       inforStudent.scoreOfPhysic +
//       inforStudent.scoreOfChermical) /
//     3
//   );
// };

tinhDTB = () => {
  let inforStudent = getInforFromForm();
  axios({
    url: `${BASE_URL}/Student`,
    method: 'get',
    data: inforStudent,
  })
    .then(function (res) {
      return (
        (inforStudent.scoreOfMath +
          inforStudent.scoreOfPhysic +
          inforStudent.scoreOfChermical) /
        3
      );
      fetchListStudent();
    })
    .catch(function (err) {});
};
