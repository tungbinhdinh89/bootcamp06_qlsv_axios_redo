// promise
// pendding : chờ
// resolve : thành công
// reject : thất bại
// get link axios
const BASE_URL = 'https://63f442adfe3b595e2ef03850.mockapi.io';

// GET DATA FROM AXIOS
console.log('yes');
fetchListStudent = () => {
  axios({
    url: `${BASE_URL}/Student`,
    method: `get`,
  })
    .then(function (res) {
      let listStudent = res.data;
      console.log('listStudent: ', listStudent);
      console.log('inforStudent.scoreOfMath: ', listStudent[0].scoreOfMath);
      renderListStudent(listStudent);
    })
    .catch(function (err) {
      console.log('err: ', err);
    });
};
fetchListStudent();

deleteStudent = (idStudent) => {
  axios({
    url: `${BASE_URL}/Student/${idStudent}`,
    method: `delete`,
  })
    .then(function (res) {
      fetchListStudent();
      console.log('res: ', res.data);
    })
    .catch(function (err) {
      console.log('err: ', err);
    });
};

editStudent = (idStudent) => {
  axios({
    url: `${BASE_URL}/Student/${idStudent}`,
    method: 'get',
  })
    .then(function (res) {
      console.log('res: ', res);
      console.log('idStudent: ', idStudent);
      let inforStudent = res.data;
      fillInforFromDataToForm(inforStudent);
    })
    .catch(function (err) {
      console.log('err: ', err);
    });
};

updateStudent = () => {
  let inforStudent = getInforFromForm();
  axios({
    url: `${BASE_URL}/Student/${inforStudent.idStudent}`,
    method: 'put',
    data: inforStudent,
  })
    .then(function (res) {
      fetchListStudent();
    })
    .catch(function (err) {});
};

addMoreStudent = () => {
  let inforStudent = getInforFromForm();

  axios({
    url: `${BASE_URL}/Student/`,
    method: `post`,
    data: inforStudent,
  })
    .then(function (res) {
      fetchListStudent();

      console.log('res: ', res);
    })
    .catch(function (err) {
      console.log('err: ', err);
    });
};
